<?php
//add_action("wp_head", "wp_head_meta");

function qrcode_gen_activate() {

  add_option( 'Activated_Plugin', 'Plugin-Slug' );

  /* activation code here */
}
register_activation_hook( QR_PLUGIN, 'qrcode_gen_activate' );

function qrcode_gen_DEactivate() {

  delete_option( 'Activated_Plugin');

  /* DEactivation code here */
}
register_deactivation_hook( QR_PLUGIN, 'qrcode_gen_DEactivate' );

function qr_head_css() {
	wp_enqueue_style( 'qr_style_bootstrap', QR_PLUGIN_DIR_HTTP.'/bootstrap.css');
}
add_action('wp_enqueue_scripts', 'qr_head_css' );

function qr_head_js() {
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'qr_function_js', QR_PLUGIN_DIR_HTTP.'/function.js');
	wp_enqueue_script( 'jqr_js', QR_PLUGIN_DIR_HTTP.'/jq_qrcode/jquery.qrcode.js');
	wp_enqueue_script( 'qrcode_js', QR_PLUGIN_DIR_HTTP.'/jq_qrcode/qrcode.js');
}
add_action('wp_enqueue_scripts', 'qr_head_js' );

function qr_ajax_data(){ 
	wp_localize_script( 'qr_function_js', 'ajax', 
		array(
			'url' => admin_url('admin-ajax.php'),
			'nonce' => wp_create_nonce('ajax-nonce')
		)
	);
}
add_action('wp_enqueue_scripts', 'qr_ajax_data', 99 ); 