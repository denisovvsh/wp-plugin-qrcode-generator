<?php

/*

Plugin Name: QR-code Generator

Plugin URI: http://

Description: Данный плагин позволяет генерировать QR-code для подключения к WiFi. Для отображения плагина: [qrcode-gen]

Version: 1.0

Author: Denisov Vadim

Author email: denisovvsh@gmail.com

	Copyright 2017  Denisov Vadim  (email: denisovvsh@gmail.com)



    This program is free software; you can redistribute it and/or modify

    it under the terms of the GNU General Public License as published by

    the Free Software Foundation; either version 2 of the License, or

    (at your option) any later version.



    This program is distributed in the hope that it will be useful,

    but WITHOUT ANY WARRANTY; without even the implied warranty of

    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the

    GNU General Public License for more details.



    You should have received a copy of the GNU General Public License

    along with this program; if not, write to the Free Software

    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*/

define( 'QR_VERSION', '1.0.1' );

define( 'QR_REQUIRED_WP_VERSION', '4.9' );

define( 'QR_PLUGIN', __FILE__ );

define( 'QR_PLUGIN_BASENAME', plugin_basename( QR_PLUGIN ) );

define( 'QR_PLUGIN_NAME', trim( dirname( QR_PLUGIN_BASENAME ), '/' ) );

define( 'QR_PLUGIN_DIR', untrailingslashit( dirname( QR_PLUGIN ) ) );

define( 'QR_PLUGIN_DIR_HTTP', plugins_url('', QR_PLUGIN));

define( 'QR_PLUGIN_DIR_HTTP_ONLY_PATH', str_replace(get_site_url(), "", QR_PLUGIN_DIR_HTTP));

require_once QR_PLUGIN_DIR . '/function.php';
require_once QR_PLUGIN_DIR . '/action-gen-qr.php';

function qrcode_gen_shortcode($attr) {
    if( !is_admin() ){
	   return apply_filters('gen_qr_hook', '');
    }
}
add_shortcode('qrcode-gen', 'qrcode_gen_shortcode');



?>