<?php
add_filter('gen_qr_hook', 'gen_qr_js');
function gen_qr_js() {
	?>
	<div class="container">
		<div class="row">
			<div class="card col-lg-12">
				<div class="card-header my-1">
				    <p class="card-text">Укажите параметры и сгенерируйте QR-code</p>
				</div>
				<div class="card-body">
					<form>
			    		<div class="form-group">
						    <input type="text" class="form-control col-sm-6" id="siid_name" placeholder="SIID имя сети" name="siid_name" value="" style="font-size:1em !important;">
					    </div>

					    <div class="form-group col-sm-6">
					    	<label for="key_pass">Тип шифрования</label>
					    	<select id="encryption_t" class="form-control">
					    		<option selected value="WPA">WPA</option>
					    		<option value="WEP">WEP</option>
					    		<option value="WPA2-EAP">WPA2-EAP</option>
					    		<option value="nopass">Без пароля</option>
					    	</select>
					    </div>

					    <div class="form-group" id="div_key_pass">
						    <input type="text" class="form-control col-sm-6" id="key_pass" placeholder="Пароль подключения к WiFi" name="key_pass" value="" style="font-size:1em !important;">
					    </div>

					    <div class="form-check my-2">
						    <input type="checkbox" class="form-check-input" id="check_hidden" name="check_hidden">
						    <label class="form-check-label" for="check_hidden">Скрытая сеть</label>
						</div>
						
						<div class="form-group">
							<button id="qr_code_generator" class="btn btn-outline-success col-sm-6" title="Generate" style="font-size:0.9em !important;">Генерировать QR</button>
						</div>
					</form>	
				</div>
				<div class="card-body">
					<div class="alert alert-warning my-2 col-sm-6" id="div_worrning_generate" style="display: none;"></div>
					<div class="p-2 text-center" id="div_result_generate" style="display: none;"></div>
				</div>
			</div>
		</div>
	</div>
	
		<script type='text/javascript'>
			jQuery('#qr_code_generator').on('click', function(event) {
				event.preventDefault(); 
				event.stopImmediatePropagation();
				jQuery('#div_result_generate').html('');
				jQuery('#div_worrning_generate').html('');
				jQuery('#div_result_generate').attr('style', 'display:none;');
				jQuery('#div_worrning_generate').attr('style', 'display:none;');
				setTimeout(function(){	
					genQRCode.create_qr();
				}, 500);
			});

			jQuery('#encryption_t').on('change', function() {
				event.stopImmediatePropagation();
				genQRCode.change_select_qr();
			});

		</script>

	<?php
	
}

if( wp_doing_ajax() ){
//	add_filter('wp_ajax_nopriv_gen_pass', 'gen_pass_callback', 10);
//	add_filter('wp_ajax_nopriv_gen_hash', 'gen_hash_callback', 10);
//	add_filter('wp_ajax_gen_pass', 'gen_pass_callback', 10);
//	add_filter('wp_ajax_gen_hash', 'gen_hash_callback', 10);
}

?>