// JavaScript Document
var genQRCode = {
	characterless_int: function(str){
		let reg = /^[\d]+$/i;
		str = str.replace(/\s+/g,"");
		return reg.test(str) ? str : false;
	},

	characterless_string: function(str){
		let reg = /^[\s\w\d_\-\:\;\(\)\.\,\!\?\"№\/\#\*\«\»\’]{1,}$/i;
		return reg.test(str) ? str : false;
	},

	create_qr: function(){
		jQuery(document).ready(function($) {
			let siid = false;
			let check_hidden = false;
			let select_encryption = 'WPA';
			let key_pass = '';
			if(!!genQRCode.characterless_string(jQuery('#siid_name').val())){
				jQuery('#div_result_generate').removeAttr('style');
				jQuery('#siid_name').attr('class', 'form-control col-sm-6');
				if (jQuery("input").is('#key_pass')) {
					jQuery('#key_pass').attr('class', 'form-control col-sm-6');
				}
				siid = jQuery('#siid_name').val()
				check_hidden = (jQuery('#check_hidden').is(":checked")) ? true : false;
				if (jQuery('#encryption_t').val() !== 'nopass') {
					select_encryption = jQuery('#encryption_t').val();
					if (!!jQuery('#key_pass').val()) {
						key_pass = jQuery('#key_pass').val();
						jQuery('#div_result_generate').removeAttr('style');
					}else{
						jQuery('#div_result_generate').attr('style', 'display:none;');
						jQuery('#div_worrning_generate').html('Введите пароль или выбирете тип шифрования - Без пароля');
						jQuery('#div_worrning_generate').removeAttr('style');
						jQuery('#key_pass').attr('class', 'form-control col-sm-6 alert alert-warning');
						return false;
					}
				}
					
			}else{
				jQuery('#div_result_generate').attr('style', 'display:none;');
				jQuery('#div_worrning_generate').html('Введите название сети - SIID (Латинскими буквами)');
				jQuery('#div_worrning_generate').removeAttr('style');
				jQuery('#siid_name').attr('class', 'form-control col-sm-6 alert alert-warning');
				return false;
			}

			let data = (!!!jQuery('#key_pass').val()) ? 'WIFI:S:'+siid+';H:'+check_hidden+';A:anon;;' : 'WIFI:S:'+siid+';T:'+select_encryption+';P:'+key_pass+';H:'+check_hidden+';;';

			jQuery('#div_result_generate').qrcode({
				'width': 200,
				'height': 200,
				'text': data
			});

//var jqElement = jQuery("#div_result_generate canvas");
//console.log(jqElement);
		});
	},

	change_select_qr: function(){

		if (jQuery('#encryption_t').val() === 'nopass') {
			jQuery('#div_key_pass').attr('style', 'display:none;');	
		}else{
			jQuery('#div_key_pass').removeAttr('style');
		}
	}
	
}
